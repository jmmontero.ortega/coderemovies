using CodereMovies.ScheduledJobsAbstractions.Jobs;
using CodereMovies.ScheduledJobsAbstractions.Models;

namespace CodereMovies.ScheduledJobsAbstractions.Interfaces;

public interface IJobSchedulerService
{
    Task<JobResult> StartJob<TJob, TJobParameters>(TJobParameters jobParameters)
        where TJob : IScheduledJob
        where TJobParameters : class;
}