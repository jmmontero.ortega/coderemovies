using CodereMovies.ScheduledJobsAbstractions.Jobs;

namespace CodereMovies.ScheduledJobsAbstractions;

public record GroupJob
{
    public string GroupName { get; set; }
    public List<Type> Jobs { get; set; }
    public EnumTriggers Trigger { get; set; }
}