namespace CodereMovies.ScheduledJobsAbstractions.Models.JobParameters;

public class LoadShowMainInformationJobParameters
{
    public int StartId { get; set; }
    public int EndId { get; set; }
}