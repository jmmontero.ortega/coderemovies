namespace CodereMovies.ScheduledJobsAbstractions.Models;

public abstract class JobResult
{
    
}

public class JobResultOK : JobResult
{
}

public class JobResultError : JobResult
{
    public string MessageError { get; set; }
}