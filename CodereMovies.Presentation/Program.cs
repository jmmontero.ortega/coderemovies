using System;
using System.Collections.Generic;
using CodereMovies.Presentation.Extensions;
using CodereMovies.Presentation.Models;
using CodereMovies.ScheduledJobs;
using CodereMovies.ScheduledJobsAbstractions;
using CodereMovies.ScheduledJobsAbstractions.Jobs;
using CodereMovies.ScheduledJobsAbstractions.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


var builder = WebApplication.CreateBuilder(args);


builder.Services.AddControllers();
builder.Services.AddSwaggerDocumentation();
builder.Services.ConfigureDatabase(builder.Configuration);
builder.Services.ConfigureIdentity(builder.Configuration);
builder.Services.Configure<JWTOptions>(builder.Configuration.GetSection(nameof(JWTOptions)));

await builder.Services.PrepareScheduledJobs(new GroupJob[]
{
    new()
    {
        GroupName = GroupNameJobs.LoadMoreInformationRequested,
        Jobs = new List<Type>()
        {
            typeof(ILoadShowMainInformationJob)
        },
        Trigger = EnumTriggers.Daily
    }
});

builder.RegisterServicesAndMappers();
    

builder.Services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
{
    builder.AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader();
}));


builder.Services.BuildServiceProvider();



var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors("MyPolicy");
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.UseSwaggerDocumentation();
app.Run();

