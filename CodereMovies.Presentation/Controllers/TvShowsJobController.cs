using System.Threading.Tasks;
using CodereMovies.Application.Models;
using CodereMovies.Models;
using CodereMovies.ScheduledJobs.Jobs;
using CodereMovies.ScheduledJobsAbstractions.Interfaces;
using CodereMovies.ScheduledJobsAbstractions.Jobs;
using CodereMovies.ScheduledJobsAbstractions.Models;
using CodereMovies.ScheduledJobsAbstractions.Models.JobParameters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CodereMovies.Presentation.Controllers;

[Route("api/[controller]")]
[Authorize]
[ApiController]
public class TvShowsJobController : ControllerBase
{
    private readonly IJobSchedulerService _jobSchedulerService;
    
    public TvShowsJobController(IJobSchedulerService jobSchedulerService)
    {
        _jobSchedulerService = jobSchedulerService;
    }
    
    [HttpPost]
    public async Task<IActionResult> RequestJobLoadMainInformation(
        [FromBody] RequestJobLoadMainInformationDTO requestJobLoadMainInformationDto)
    {
        if (requestJobLoadMainInformationDto.StartId > requestJobLoadMainInformationDto.EndId)
        {
            return StatusCode(StatusCodes.Status400BadRequest, ErrorMessages.WrongUpperId);
        }
        
        var jobResult =
            await _jobSchedulerService.StartJob<LoadShowMainInformationJob, LoadShowMainInformationJobParameters>(
                new LoadShowMainInformationJobParameters()
                {
                    StartId = requestJobLoadMainInformationDto.StartId,
                    EndId = requestJobLoadMainInformationDto.EndId
                });

        return jobResult switch
        {
            JobResultOK => Ok(),
            JobResultError error => StatusCode(StatusCodes.Status500InternalServerError, error.MessageError),
            _ => StatusCode(StatusCodes.Status500InternalServerError, ErrorMessages.ErrorNotRecognized)
        };
    }
}