using System.Threading.Tasks;
using CodereMovies.Infraestructure.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CodereMovies.Presentation.Controllers;

[Route("api/[controller]")]
[Authorize]
[ApiController]
public class TvShowsController : ControllerBase
{
    private readonly ITvMazeReadService _tvMazeReadService;
    
    public TvShowsController(ITvMazeReadService tvMazeReadService)
    {
        _tvMazeReadService = tvMazeReadService;
    }
    
    [HttpGet]
    public async Task<IActionResult> TvShows(int id)
    {
        var tvShow = await _tvMazeReadService.LoadTvMainShow(id);

        return tvShow != null
            ? Ok(tvShow)
            : NoContent();
    }
}