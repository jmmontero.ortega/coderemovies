using System.Text;
using CodereMovies.Infraestructure;
using CodereMovies.Infraestructure.Models;
using CodereMovies.Presentation.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CodereMovies.Presentation.Extensions;

public static class ServiceExtensions
{
    public static void ConfigureDatabase(this IServiceCollection services, IConfiguration Configuration)
    {
        services.AddDbContext<AppDbContext>(opt =>
            opt.UseSqlite(Configuration.GetConnectionString("WebApiDatabase")));
    }

    public static void ConfigureIdentity(this IServiceCollection services, IConfiguration Configuration)
    {

        var jwtOptions = Configuration.GetSection(nameof(JWTOptions)).Get<JWTOptions>();
        services.AddIdentity<User, Role>()
            .AddEntityFrameworkStores<AppDbContext>()
            .AddDefaultTokenProviders();
        services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                {
                    ValidAudiences = jwtOptions.ValidAudiences,
                    ValidIssuer= jwtOptions.ValidIssuer,
                    ValidateIssuer = false,
                    ValidateAudience = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Secret))
                };
            });
    }
}