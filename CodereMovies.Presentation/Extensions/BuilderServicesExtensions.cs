using System;
using CodereMovies.Infraestructure;
using CodereMovies.Infraestructure.Interfaces;
using CodereMovies.Infraestructure.Services;
using CodereMovies.ScheduledJobs.Services;
using CodereMovies.ScheduledJobsAbstractions.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace CodereMovies.Presentation.Extensions;

public static class BuilderServicesExtensions
{

    public static WebApplicationBuilder RegisterServicesAndMappers(this WebApplicationBuilder builder)
        => builder
            .RegisterCommonServices()
            .RegisterHttpClients()
            .RegisterSingletons()
            .RegisterServices()
            .RegisterMappers();

    private static WebApplicationBuilder RegisterCommonServices(this WebApplicationBuilder builder)
    {
        builder.Services.TryAddSingleton<IDateService, DateService>();
        
        builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
        
        builder.Services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        builder.Services.TryAddSingleton<ICacheContext, CacheContext>();

        return builder;
    }

    private static WebApplicationBuilder RegisterHttpClients(this WebApplicationBuilder builder)
    {
        builder.Services.AddHttpClient(nameof(TvMazeService), c =>
        {
            c.BaseAddress = new Uri("https://api.tvmaze.com/");
        });

        return builder;
    }

    private static WebApplicationBuilder RegisterSingletons(this WebApplicationBuilder builder)
    {
        builder.Services.TryAddSingleton<IJobSchedulerService, JobSchedulerService>();
        return builder;
    }

    private static WebApplicationBuilder RegisterServices(this WebApplicationBuilder builder)
    {
        builder.Services.AddScoped<ITvMazeService, TvMazeService>();
        builder.Services.AddScoped<ITvMazeReadService, TvMazeReadService>();

        return builder;
    }

    private static WebApplicationBuilder RegisterMappers(this WebApplicationBuilder builder)
    {
        /*TODO: Add mappers here... 
        builder.Services.AddAutoMapper(typeof(TodoProfileDTO));
        */
        return builder;
    }
}