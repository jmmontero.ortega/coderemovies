using System.Net;
using CodereMovies.Infraestructure.Interfaces;
using CodereMovies.Infraestructure.Services;
using Microsoft.Extensions.Logging;
using Moq;
using RichardSzalay.MockHttp;

namespace CodereMovies.Infraestructure.Tests;

public class TvMazeServiceTests
{
    private readonly ITvMazeService _tvMazeService;
    private readonly MockHttpMessageHandler _mockHttpMessageHandler;
    private readonly Mock<ILogger<ITvMazeService>> _mockLogger;

    public TvMazeServiceTests()
    {
        _mockHttpMessageHandler = new MockHttpMessageHandler();
        
        var httpClient = new HttpClient(_mockHttpMessageHandler);
        httpClient.BaseAddress = new Uri("https://api.tvmaze.com/");
        
        var mockClientFactory = new Mock<IHttpClientFactory>();
        mockClientFactory.Setup(x => x.CreateClient(It.IsAny<string>()))
            .Returns(() => httpClient);

        _mockLogger = new Mock<ILogger<ITvMazeService>>();
        
        _tvMazeService = new TvMazeService(
            _mockLogger.Object,
            mockClientFactory.Object);
    }
    
    
    
    [Fact]
    public async Task run_job_start_with_id_greater_than_end_id_throws_an_exception()
    {
        await Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () =>
        {
            await _tvMazeService.LoadMainInformation(100, 50);
        });
    }

    [Fact]
    public async Task run_job_ok()
    {
        _mockHttpMessageHandler.When("https://api.tvmaze.com/shows/*")
            .Respond("application/json", JsonApiContent);
        
        var tvShows = await _tvMazeService.LoadMainInformation(0, 50);
        
        Assert.Equal(50, tvShows.Count);
    }

    [Fact]
    public async Task run_job_but_respond_with_error()
    {
        _mockHttpMessageHandler.When("https://api.tvmaze.com/shows/*")
            .Respond(HttpStatusCode.Unauthorized);
        
        var thShows = await _tvMazeService.LoadMainInformation(0, 50);
        
        _mockLogger.Verify(
            logger => logger.Log(
                It.Is<LogLevel>(logLevel => logLevel == LogLevel.Error),
                It.Is<EventId>(eventId => eventId.Id == 1),
                It.Is<It.IsAnyType>((@object, @type) => true),
                It.IsAny<Exception>(),
                It.IsAny<Func<It.IsAnyType, Exception?, string>>()),
            Times.AtLeast(50));
        
        Assert.Empty(thShows);
    }

    private const string JsonApiContent =
        "{\"id\":1,\"url\":\"https://www.tvmaze.com/shows/1/under-the-dome\",\"name\":\"Under the Dome\",\"type\":\"Scripted\",\"language\":\"English\",\"genres\":[\"Drama\",\"Science-Fiction\",\"Thriller\"],\"status\":\"Ended\",\"runtime\":60,\"averageRuntime\":60,\"premiered\":\"2013-06-24\",\"ended\":\"2015-09-10\",\"officialSite\":\"http://www.cbs.com/shows/under-the-dome/\",\"schedule\":{\"time\":\"22:00\",\"days\":[\"Thursday\"]},\"rating\":{\"average\":6.5},\"weight\":98,\"network\":{\"id\":2,\"name\":\"CBS\",\"country\":{\"name\":\"United States\",\"code\":\"US\",\"timezone\":\"America/New_York\"},\"officialSite\":\"https://www.cbs.com/\"},\"webChannel\":null,\"dvdCountry\":null,\"externals\":{\"tvrage\":25988,\"thetvdb\":264492,\"imdb\":\"tt1553656\"},\"image\":{\"medium\":\"https://static.tvmaze.com/uploads/images/medium_portrait/81/202627.jpg\",\"original\":\"https://static.tvmaze.com/uploads/images/original_untouched/81/202627.jpg\"},\"summary\":\"<p><b>Under the Dome</b> is the story of a small town that is suddenly and inexplicably sealed off from the rest of the world by an enormous transparent dome. The town's inhabitants must deal with surviving the post-apocalyptic conditions while searching for answers about the dome, where it came from and if and when it will go away.</p>\",\"updated\":1631010933,\"_links\":{\"self\":{\"href\":\"https://api.tvmaze.com/shows/1\"},\"previousepisode\":{\"href\":\"https://api.tvmaze.com/episodes/185054\"}}}";
}