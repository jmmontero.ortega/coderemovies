using CodereMovies.Domain;
using CodereMovies.Infraestructure.Interfaces;
using CodereMovies.Infraestructure.Services;
using Moq;

namespace CodereMovies.Infraestructures.UnitTests;

public class TvMazeReadServiceTests
{
    private readonly ITvMazeReadService _tvMazeReadService;
    private readonly Mock<IUnitOfWork> _mockUnitOfWork;
    private readonly Mock<ITvMazeService> _mockTvMazeService;
    
    public TvMazeReadServiceTests()
    {
        _mockUnitOfWork = new Mock<IUnitOfWork>();
        _mockTvMazeService = new Mock<ITvMazeService>();

        _tvMazeReadService = new TvMazeReadService(
            _mockUnitOfWork.Object,
            _mockTvMazeService.Object
        );
    }

    [Fact]
    public async Task request_a_show_and_work_correctly()
    {
        _mockUnitOfWork.Setup(tvShow => tvShow.TvShows.FindById(It.IsAny<int>()))
            .Returns(ContentTvShow("Repository"));

        _mockTvMazeService.Setup(tvShow => tvShow.LoadMainInformation(It.IsAny<int>()))
            .ReturnsAsync(ContentTvShow("Api"));

        var tvShow = await _tvMazeReadService.LoadTvMainShow(1);
        
        Assert.NotNull(tvShow);
    }

    [Fact]
    public async Task request_a_show_and_repository_answer()
    {
        _mockUnitOfWork.Setup(tvShow => tvShow.TvShows.FindById(It.IsAny<int>()))
            .Returns(ContentTvShow("Repository"));

        _mockTvMazeService.Setup(tvShow => tvShow.LoadMainInformation(It.IsAny<int>()))
            .ReturnsAsync((TvShow?)null);
        
        var tvShow = await _tvMazeReadService.LoadTvMainShow(1);
        
        Assert.NotNull(tvShow);
        Assert.Contains("Repository", tvShow.name);
    }

    [Fact]
    public async Task request_a_show_and_api_answer()
    {
        _mockUnitOfWork.Setup(tvShow => tvShow.TvShows.FindById(It.IsAny<int>()))
            .Returns((TvShow?)null);

        _mockTvMazeService.Setup(tvShow => tvShow.LoadMainInformation(It.IsAny<int>()))
            .ReturnsAsync(ContentTvShow("Api"));
        
        var tvShow = await _tvMazeReadService.LoadTvMainShow(1);
        
        Assert.NotNull(tvShow);
        Assert.Contains("Api", tvShow.name);
    }

    [Fact]
    public async Task request_a_show_but_nobody_answer()
    {
        _mockUnitOfWork.Setup(tvShow => tvShow.TvShows.FindById(It.IsAny<int>()))
            .Returns((TvShow?)null);

        _mockTvMazeService.Setup(tvShow => tvShow.LoadMainInformation(It.IsAny<int>()))
            .ReturnsAsync((TvShow?)null);

        await Assert.ThrowsAsync<ArgumentNullException>(async () =>
        {
            await _tvMazeReadService.LoadTvMainShow(1);
        });
    }

    private TvShow ContentTvShow(string? from)
        => new TvShow()
        {
            id = 1,
            name = $"Content Tv show {from}"
        };

}