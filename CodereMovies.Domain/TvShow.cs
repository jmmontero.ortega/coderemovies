using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace CodereMovies.Domain;

[PrimaryKey(nameof(Id))]
public class TvShow : BaseEntity
{
    public string url { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string language { get; set; }
    public List<string> genres { get; set; }
    public string status { get; set; }
    public int runtime { get; set; }
    public int averageRuntime { get; set; }
    public string premiered { get; set; }
    public string ended { get; set; }
    public string officialSite { get; set; }
    public Schedule schedule { get; set; }
    public Rating rating { get; set; }
    public int weight { get; set; }
    public Network network { get; set; }
    public string webChannel { get; set; }
    public string dvdCountry { get; set; }
    public Externals externals { get; set; }
    public Image image { get; set; }
    public string summary { get; set; }
    public int updated { get; set; }
    public Links _links { get; set; }

    public static TvShow Empty() => new TvShow();

    public bool IsEmpty()
        => Equals(this, new TvShow());

    public override int GetHashCode()
        => Id.GetHashCode();

    public override bool Equals(object? obj)
    {
        if (obj == null)
            return false;

        if (obj.GetType() != typeof(TvShow))
            return false;

        var hashCode = GetHashCode();
        var objHashCode = ((obj as TvShow)!).GetHashCode();

        return hashCode == objHashCode;
    }
}

[PrimaryKey(nameof(Id))]
public class Schedule : BaseEntity
{
    
    public string time { get; set; }
    public List<string> days { get; set; }
}

[PrimaryKey(nameof(Id))]
public class Rating: BaseEntity
{
    public double average { get; set; }
}

[PrimaryKey(nameof(Id))]
public class Network: BaseEntity
{
    public string name { get; set; }
    public Country country { get; set; }
    public string officialSite { get; set; }
}

[PrimaryKey(nameof(Id))]
public class Country : BaseEntity
{
    public string name { get; set; }
    public string code { get; set; }
    public string timezone { get; set; }
}

[PrimaryKey(nameof(Id))]
public class Externals: BaseEntity
{
    public int tvrage { get; set; }
    public int thetvdb { get; set; }
    public string imdb { get; set; }
}

[PrimaryKey(nameof(Id))]
public class Image: BaseEntity
{
    public string medium { get; set; }
    public string original { get; set; }
}

[PrimaryKey(nameof(Id))]
public class Links: BaseEntity
{
    public Self self { get; set; }
    public PreviousEpisode PreviousEpisode { get; set; }
}

[PrimaryKey(nameof(Id))]
public class Self: BaseEntity
{
    public string href { get; set; }
}

[PrimaryKey(nameof(Id))]
public class PreviousEpisode: BaseEntity
{
    public string href { get; set; }
}

