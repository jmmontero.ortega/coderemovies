# Introduction

There are total 5 branchs:

- **main**. All the code
- **feature/1-AddJob**. With first assignment
- **feature/2-CreateEndPointToCallJob**. With second assignment
- **feature/3-CreateQueryToReadTvShow**. With third assigmnent
- **feature/4-CleanUp**. Where clean all and add last details

# How start?

App starts in swagger documentation, you can login with SuperAdmin/SuperAdmin007!****


# First Assignment

Create a scheduling job using Quartz.Net is a library better documented, there are a good support and 
is open source.

Anyway we create a new project that working in an abstraction if in any moment refactor to another way
to implement 

The job store directly in memory, I will create a small cache context to store here. Persist data inside 
of database require tables special tables because that provoke dead locks. 

Is true the database for the moment it's readonly, however, the API should respond more quickly and then if
store in the database. 

### Why add SerializerUtils?

Serializing / Deserializing actions is really common, so create an abstraction to concentrate all calls
allows create new changes or add more secure options inside.

# Second assignment

Because in the first assignment put the job to execute daily. In this second job, we move to at request. 
So we refactor for the controller. Create an abstraction for scheduler to continue with our abstraction for
Scheduled jobs.

# Third assignment

Create a new controller to separate for jobs request and also a new service only for reading operations 

# Clean up

### EF SQLite 

- Serialization for List<string> database.

Not it's the properly way to store all the data related with List<string> in a relational database, but working for the assignment.
The correct way it's create a new table or use a database in production that allow not relational models.

- Serialization insensitive

Use SerializationUtils save me, when create the TvShowMazeService I suppose I will have this problem but thanks this abstraction I 
fix quickly.

- SQLite EnsureDeleted/EnsureCreated in AppDbContext.

Related with:
https://www.talkingdotnet.com/create-sqlite-db-entity-framework-core-code-first/

SQLite don't allow Code first

# Contexts

I follow this instructions to create the webapi from scratch.

https://errorexceptionfix.com/create-a-good-ddd-architecture-in-net-7/

### Why create models project?

Because this it's a web api so, we need to consume. So we create a internal nuget package to consume
for another apps. i.e Xamarin or WebApp, also we create a new project with client to consume.