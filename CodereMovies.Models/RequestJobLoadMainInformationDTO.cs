namespace CodereMovies.Models;

public class RequestJobLoadMainInformationDTO
{
    public int StartId { get; set; }
    public int EndId { get; set; }
}