using CodereMovies.Infraestructure.Interfaces;
using CodereMovies.Infraestructure.Repositories;

namespace CodereMovies.Infraestructure;

public class UnitOfWork : IUnitOfWork
{
    private readonly AppDbContext _dbContext;
    private readonly IDateService _dateService;
    private readonly ICacheContext _cacheContext;
    
    public UnitOfWork(
        AppDbContext dbContext,
        IDateService dateService,
        ICacheContext cacheContext)
    {
        _dbContext = dbContext;
        _dateService = dateService;
        _cacheContext = cacheContext;
    }

    private IRepositoryTvShows _repositoryTvShows;
    public IRepositoryTvShows TvShows
    {
        get
        {
            if (_repositoryTvShows == null)
            {
                _repositoryTvShows = new RepositoryTvShows(_dbContext, _dateService, _cacheContext);
            }
            
            return _repositoryTvShows;
        }
    }

    public void Save()
    {
        _dbContext.SaveChanges();
    }
}