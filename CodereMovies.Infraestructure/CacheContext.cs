using CodereMovies.Domain;
using CodereMovies.Infraestructure.Interfaces;

namespace CodereMovies.Infraestructure;

public class CacheContext : ICacheContext
{
    public List<TvShow> TvShows { get; set; } = new List<TvShow>();
    
    
}