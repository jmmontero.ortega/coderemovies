﻿using System.Reflection;
using CodereMovies.Domain;
using CodereMovies.Extensions;
using CodereMovies.Infraestructure.Extensions;
using CodereMovies.Infraestructure.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CodereMovies.Infraestructure;

public class AppDbContext : IdentityDbContext<User, Role, int>
{
    private bool _created;
    public DbSet<TvShow> TvShows { get; set; }

    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
        if (!_created)
        {
            _created = true;
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }
    }

    

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(
            Assembly.GetExecutingAssembly(),
            t => t.GetInterfaces().Any(i =>
                i.IsGenericType &&
                i.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>) &&
                typeof(BaseEntity).IsAssignableFrom(i.GenericTypeArguments[0]))
        );

        builder.Entity<TvShow>()
            .Property(p => p.genres)
            .HasConversion(
                v => SerializationUtils.Serialize(v),
                v => SerializationUtils.Deserialize<List<string>>(v) ?? new List<string>());
        
        builder.Entity<Schedule>()
            .Property(p => p.days)
            .HasConversion(
                v => SerializationUtils.Serialize(v),
                v => SerializationUtils.Deserialize<List<string>>(v) ?? new List<string>());

        
        builder.SeedUsers();
        base.OnModelCreating(builder);
    }
    public override int SaveChanges()
    {
        ChangeTracker.DetectChanges();
        var markedAsModified = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified);
        var markedAsAdded = ChangeTracker.Entries().Where(x => x.State == EntityState.Added);
        foreach (var item in markedAsModified)
        {
            if (item.Entity is BaseEntity entity)
            {
                entity.ModifiedAt = DateTime.UtcNow;
            }
        }
        foreach (var item in markedAsAdded)
        {
            if (item.Entity is BaseEntity entity)
            {
                entity.CreatedAt = DateTime.UtcNow;
            }
        }
        return base.SaveChanges();
    }
}