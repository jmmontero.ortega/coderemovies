using CodereMovies.Domain;
using CodereMovies.Infraestructure.Interfaces;

namespace CodereMovies.Infraestructure.Repositories;

public class RepositoryTvShows : RepositoryBase<TvShow>, IRepositoryTvShows
{
    private readonly ICacheContext _cacheContext;
    
    public RepositoryTvShows(AppDbContext appDbContext,
        IDateService dateService,
        ICacheContext cacheContext)
        : base(appDbContext, dateService)
    {
        _cacheContext = cacheContext;
    }
    
    public override IQueryable<TvShow> FindAll()
    {
        var tvShows = _cacheContext.TvShows;
        
        base.Create(tvShows);

        return tvShows.AsQueryable();
    }

    public override TvShow? FindById(int id)
    {
        var tvShow = _cacheContext.TvShows.FirstOrDefault(tvShow => tvShow.Id == id);
        if (tvShow == null)
        {
            tvShow =  base.FindById(id);    
        }
        
        if (tvShow != null)
        {
            base.Create(tvShow);
        }

        return tvShow;
    }

    public override void Create(TvShow entity)
        => _cacheContext.TvShows.Add(entity);
    
}