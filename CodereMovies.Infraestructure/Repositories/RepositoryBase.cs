using System.Linq.Expressions;
using CodereMovies.Domain;
using CodereMovies.Infraestructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CodereMovies.Infraestructure.Repositories;

public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : BaseEntity
{

    private readonly AppDbContext _dbContext;
    private readonly IDateService _dateService;
    
    public RepositoryBase(
        AppDbContext appDbContext,
        IDateService dateService)
    {
        _dbContext = appDbContext;
        _dateService = dateService;
    }
    
    protected AppDbContext DbContext => _dbContext;

    public virtual IQueryable<T> FindAll()
        => DbContext.Set<T>().AsNoTracking();

    public virtual IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        => DbContext.Set<T>()
            .Where(expression)
            .AsNoTracking();

    public virtual T? FindById(int id)
        => DbContext.Set<T>()
            .FirstOrDefault(item => item.Id == id);
    
    public virtual void Create(T entity)
        => DbContext.Set<T>()
            .AddAsync(entity);

    public virtual void Create(List<T> entities)
        => DbContext.Set<T>()
            .AddRangeAsync(entities);

    public virtual void Update(T entity)
        => DbContext.Set<T>()
            .Update(entity);

    public virtual void Delete(T entity)
        => DbContext.Set<T>()
            .Remove(entity);

    public virtual void SoftDelete(T entity)
    {
        entity.DeletedAt = _dateService.Today();
        DbContext.Update(entity);
    }
}