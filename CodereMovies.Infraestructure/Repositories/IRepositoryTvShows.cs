using CodereMovies.Domain;
using CodereMovies.Infraestructure.Interfaces;

namespace CodereMovies.Infraestructure.Repositories;

public interface IRepositoryTvShows : IRepositoryBase<TvShow>
{
    
}