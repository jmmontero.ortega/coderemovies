

namespace CodereMovies.Infraestructure.Interfaces;

public interface IDateService
{
    DateTime Today();
}