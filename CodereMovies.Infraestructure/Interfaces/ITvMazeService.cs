using CodereMovies.Domain;

namespace CodereMovies.Infraestructure.Interfaces;

public interface ITvMazeService
{
    Task<TvShow?> LoadMainInformation(int id);

    Task<List<TvShow?>> LoadMainInformation(int startId, int endId);


}