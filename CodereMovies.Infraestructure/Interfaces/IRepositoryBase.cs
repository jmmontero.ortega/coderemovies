using System.Linq.Expressions;

namespace CodereMovies.Infraestructure.Interfaces;

public interface IRepositoryBase<T> where T : class
{
    IQueryable<T> FindAll();

    IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);

    T? FindById(int id);
    
    void Create(T entity);

    void Create(List<T> entities);

    void Update(T entity);

    void Delete(T entity);

    void SoftDelete(T entity);
}