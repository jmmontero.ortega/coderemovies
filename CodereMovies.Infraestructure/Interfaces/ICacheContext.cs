using CodereMovies.Domain;

namespace CodereMovies.Infraestructure.Interfaces;

public interface ICacheContext
{
    List<TvShow> TvShows { get; set; }
    
}