using CodereMovies.Domain;

namespace CodereMovies.Infraestructure.Interfaces;

public interface ITvMazeReadService
{
    Task<TvShow?> LoadTvMainShow(int id);
}