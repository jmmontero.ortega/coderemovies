using CodereMovies.Infraestructure.Repositories;

namespace CodereMovies.Infraestructure.Interfaces;

public interface IUnitOfWork
{
    IRepositoryTvShows TvShows { get; }
    
    void Save();
}