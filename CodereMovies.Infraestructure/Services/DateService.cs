
using CodereMovies.Infraestructure.Interfaces;

namespace CodereMovies.Infraestructure.Services;

public class DateService : IDateService
{
    public DateTime Today()
        => DateTime.Today;
}