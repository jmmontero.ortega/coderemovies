using System.Text.Json;
using CodereMovies.Application.Models;
using CodereMovies.Domain;
using CodereMovies.Extensions;
using CodereMovies.Infraestructure.Interfaces;
using Microsoft.Extensions.Logging;


namespace CodereMovies.Infraestructure.Services;

public class TvMazeService : ITvMazeService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger _logger;
    
    public TvMazeService(
        ILogger<ITvMazeService> logger,
        IHttpClientFactory clientFactory)
    {
        _logger = logger;
        _httpClient = clientFactory.CreateClient(nameof(TvMazeService));
    }
    
    public async Task<TvShow?> LoadMainInformation(int id)
    {
        try
        {
            var message = await _httpClient.GetAsync($"/shows/{id}");

            if (message.IsSuccessStatusCode)
            {
                var content = await message.Content.ReadAsStringAsync();
                return SerializationUtils.Deserialize<TvShow>(content);
            }
            else
            {
                _logger.LogHttpMessageException(message.StatusCode, $"/shows/{id} returns error");
            }
        }
        catch (JsonException jsonException)
        {
            _logger.LogException(jsonException);
        }
        catch (Exception e)
        {
            _logger.LogException(e);
            throw;
        }

        return TvShow.Empty();

    }

    public async Task<List<TvShow?>> LoadMainInformation(int startId, int endId)
    {
        if (startId > endId)
            throw new ArgumentOutOfRangeException(ErrorMessages.WrongUpperId);
        
        var tvShows = new List<TvShow?>();

        for (var currentId = startId; currentId < endId; currentId++)
        {
            tvShows.Add(await LoadMainInformation(currentId));
        }

        return tvShows
            .Where(tvShow => tvShow != null && !tvShow.IsEmpty())
            .ToList();
    }

    
}