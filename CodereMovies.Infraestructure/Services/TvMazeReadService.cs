using CodereMovies.Domain;
using CodereMovies.Infraestructure.Interfaces;

namespace CodereMovies.Infraestructure.Services;

public class TvMazeReadService : ITvMazeReadService
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly ITvMazeService _tvMazeService;

    public TvMazeReadService(
        IUnitOfWork unitOfWork,
        ITvMazeService tvMazeService)
    {
        _unitOfWork = unitOfWork; 
        _tvMazeService = tvMazeService;
    }
    
    public async Task<TvShow?> LoadTvMainShow(int id)
    {
        var tvShow = _unitOfWork.TvShows.FindById(id);

        if (tvShow != null)
        {
            return tvShow;
        }

        tvShow = await _tvMazeService.LoadMainInformation(id);
        if (tvShow != null)
        {
            _unitOfWork.TvShows.Create(tvShow);
            return tvShow;
        }
        
        

        throw new ArgumentNullException($"Id not return any tv show");
    }
}