using System.Text.Json;

namespace CodereMovies.Extensions;

public static class SerializationUtils
{
    public static string Serialize<T>(T obj) 
        => JsonSerializer.Serialize<T>(obj, new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true
        });

    public static T? Deserialize<T>(string content) 
        => JsonSerializer.Deserialize<T>(content, new JsonSerializerOptions()
        {
            PropertyNameCaseInsensitive = true
        });
}