﻿namespace CodereMovies.Extensions;

public static class CollectionExtensions
{
    public static void Foreach<T>(this IEnumerable<T> collection, Action<T> executeAction)
    {
        foreach (var element in collection)
        {
            executeAction.Invoke(element);
        }
    }
    
}