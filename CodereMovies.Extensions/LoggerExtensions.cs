

using System.Net;
using Microsoft.Extensions.Logging;

namespace CodereMovies.Extensions;

public static class LoggerExtensions
{
    private static readonly EventId ErrorEventId = new EventId(0);
    private static readonly EventId HttpMessageError = new EventId(1);
    private static readonly EventId JobEventId = new EventId(2);
    
    public static void LogException(this ILogger logger, Exception e)
        => logger.LogError(ErrorEventId, e, string.Empty);

    public static void LogHttpMessageException(this ILogger logger, HttpStatusCode statusCode, string request)
        => logger.LogError(HttpMessageError, $"{request} returns {statusCode}");

    public static void LogJobInformation(this ILogger logger, string message)
        => logger.LogInformation(JobEventId, message);


}