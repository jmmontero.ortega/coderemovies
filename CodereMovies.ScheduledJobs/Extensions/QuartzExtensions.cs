using CodereMovies.ScheduledJobs.Jobs;
using CodereMovies.ScheduledJobsAbstractions;
using CodereMovies.ScheduledJobsAbstractions.Jobs;
using Quartz;


namespace CodereMovies.ScheduledJobs.Extensions;

public static class QuartzExtensions
{
    public static IServiceCollectionQuartzConfigurator AddJobs(this IServiceCollectionQuartzConfigurator options, GroupJob groupJob)
    {
        foreach (var job in groupJob.Jobs)
        {
            options.AddScheduledJob(job, 
                j => j.WithIdentity(groupJob.GroupName));
        }

        options.AddTriggerScheduled(groupJob.Trigger,
            t => t
                .WithIdentity(groupJob.Trigger.ToString())
                .ForJob(groupJob.GroupName));
        
        return options;
    }

    private static IServiceCollectionQuartzConfigurator AddScheduledJob(
        this IServiceCollectionQuartzConfigurator configurator, Type scheduledJob,
        Action<IJobConfigurator> withConfiguration)
    {
        if (scheduledJob == typeof(ILoadShowMainInformationJob))
        {
            return configurator.AddJob<LoadShowMainInformationJob>(withConfiguration);
        }

        throw new ArgumentException($"Job not identified {scheduledJob.Name}");
    }
    
    private static IServiceCollectionQuartzConfigurator AddTriggerScheduled(
        this IServiceCollectionQuartzConfigurator configurator, 
        EnumTriggers enumTrigger, 
        Func<ITriggerConfigurator, ITriggerConfigurator> withConfiguration) 
        => enumTrigger switch
        {
            EnumTriggers.Daily => DailyTrigger(configurator, withConfiguration),
            EnumTriggers.AtRequest => AtRequestTrigger(configurator, withConfiguration),
            _ => throw new ArgumentNullException($"Trigger not recognized {enumTrigger}")
        };
    
    private static IServiceCollectionQuartzConfigurator DailyTrigger(
        this IServiceCollectionQuartzConfigurator configurator,
        Func<ITriggerConfigurator, ITriggerConfigurator> withConfiguration)
        => configurator.AddTrigger(t =>
            withConfiguration(t)
                .StartNow()
                .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(23, 0))
        );

    private static IServiceCollectionQuartzConfigurator AtRequestTrigger(
        IServiceCollectionQuartzConfigurator configurator,
        Func<ITriggerConfigurator, ITriggerConfigurator> withConfiguration)
        => configurator.AddTrigger(t =>
            withConfiguration(t));

}