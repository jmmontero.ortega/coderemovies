using CodereMovies.Extensions;
using CodereMovies.ScheduledJobs.Extensions;
using CodereMovies.ScheduledJobsAbstractions;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.AspNetCore;
using Quartz.Impl;

namespace CodereMovies.ScheduledJobs;

public static class RunJobs
{
    public static async Task PrepareScheduledJobs(this IServiceCollection serviceCollection, GroupJob[] groupJobs)
    {
        serviceCollection.AddQuartz(q =>
        {
            q.SchedulerId = "CodereMoviesScheduler";
            q.SchedulerName = "CodereMovies Scheduler";
            groupJobs.Foreach(groupJob => q.AddJobs(groupJob));
        });

        serviceCollection.AddQuartzServer(options =>
        {
            options.WaitForJobsToComplete = true;
        });
    }

    
}