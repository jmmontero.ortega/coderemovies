using CodereMovies.Domain;
using CodereMovies.Extensions;
using CodereMovies.Infraestructure.Interfaces;
using CodereMovies.ScheduledJobsAbstractions;
using CodereMovies.ScheduledJobsAbstractions.Jobs;
using CodereMovies.ScheduledJobsAbstractions.Models.JobParameters;
using Microsoft.Extensions.Logging;
using Quartz;

namespace CodereMovies.ScheduledJobs.Jobs;

public class LoadShowMainInformationJob : ILoadShowMainInformationJob, IJob
{
    private const int ShowRequested = 500;
    
    private readonly ILogger _logger;
    private readonly ITvMazeService _tvMazeService;
    private readonly ICacheContext _cacheContext;
    
    public LoadShowMainInformationJob(
        ILogger<LoadShowMainInformationJob> logger,
        ITvMazeService tvMazeService,
        ICacheContext cacheContext)
    {
        _logger = logger;
        _tvMazeService = tvMazeService;
        _cacheContext = cacheContext;
    }
    
    public async Task Execute(IJobExecutionContext context)
    {
        if (context.Trigger.JobDataMap.Count == 0)
        {
            await RunningDaily();
        }
        else if(context.Trigger.JobDataMap.Count == 1)
        {
            await RunningAtRequest(context);
        }
        
    }
    
    private async Task RunningDaily()
    {
        var showsPerTask = ShowRequested / 5;

        if (showsPerTask > 0)
        {
            _logger.LogJobInformation($"Start job {nameof(LoadShowMainInformationJob)}");

            var loadTasks = new List<Task>();
            for (var i = 0; i < 5; i++)
            {
                var startShowId = showsPerTask * i;
                var endShowId = startShowId + showsPerTask;
                loadTasks.Add(Task.Run(async () =>
                {
                    var tvShows = await _tvMazeService.LoadMainInformation(startShowId, endShowId);
                    _cacheContext.TvShows.AddRange(tvShows);
                }));
            }

            await Task.WhenAll(loadTasks);

            _logger.LogJobInformation($"Job finished {nameof(LoadShowMainInformationJob)}");
        }
    }
    
    private async Task RunningAtRequest(IJobExecutionContext context)
    {
        var parameterJob = context.Trigger.JobDataMap.Get("Parameters") as LoadShowMainInformationJobParameters;

        if (parameterJob != null)
        {
            _logger.LogJobInformation($"Start job {nameof(LoadShowMainInformationJob)}");
            
            var tvShows = await _tvMazeService.LoadMainInformation(parameterJob.StartId, parameterJob.EndId);
            _cacheContext.TvShows.AddRange(tvShows);
            
            _logger.LogJobInformation($"Job finished {nameof(LoadShowMainInformationJob)}");
        }
        else
        {
            _logger.LogJobInformation($"Job not started {nameof(LoadShowMainInformationJob)}");
        }
        
        


    }
}