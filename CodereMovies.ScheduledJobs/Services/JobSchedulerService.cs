using CodereMovies.Extensions;
using CodereMovies.ScheduledJobsAbstractions.Interfaces;
using CodereMovies.ScheduledJobsAbstractions.Jobs;
using CodereMovies.ScheduledJobsAbstractions.Models;
using Microsoft.Extensions.Logging;
using Quartz;

namespace CodereMovies.ScheduledJobs.Services;

public class JobSchedulerService : IJobSchedulerService
{
    private readonly ILogger<JobSchedulerService> _logger;
    private readonly ISchedulerFactory _schedulerFactory;

    public JobSchedulerService(
        ILogger<JobSchedulerService> logger,
        ISchedulerFactory schedulerFactory)
    {
        _logger = logger;
        _schedulerFactory = schedulerFactory;
    }
     
    public async Task<JobResult> StartJob<TJob, TJobParameters>(TJobParameters jobParameters) 
        where TJob : IScheduledJob 
        where TJobParameters : class
    {
        var parameters = new Dictionary<string, object>()
        {
            { "Parameters", jobParameters}
        };

        try
        {
            var scheduler = await _schedulerFactory.GetScheduler();
            await scheduler.TriggerJob(
                new JobKey(GroupNameJobs.LoadMoreInformationRequested),
                new JobDataMap((IDictionary<string, object>)parameters));
        }
        catch (Exception e)
        {
            _logger.LogException(e);
            return new JobResultError() { MessageError = e.Message };
        }

        return new JobResultOK();
    }
}