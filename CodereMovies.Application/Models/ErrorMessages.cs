namespace CodereMovies.Application.Models;

public static class ErrorMessages
{
    public static string WrongUpperId = "Start id can't be greater than end id";

    public static string ErrorNotRecognized = "Error not recognized";
}